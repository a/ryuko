require "./spec_helper"
require "../src/effect"

describe "Effect" do
  it "parses" do
    parser = Effect::Parser.new
    list = parser.parse("a b c d e")
    list.commands.size.should eq 5
  end

  it "doesnt parse on bad input" do
    parser = Effect::Parser.new
    begin
      list = parser.parse("a(1")
    rescue ex : Effect::ParseError
    end
  end

  it "doesnt parse on more than 10 effects" do
    parser = Effect::Parser.new
    begin
      list = parser.parse("a b c d e f g h i j k l m n o p q")
    rescue ex : Effect::ParseError
    end
  end

  it "doesnt parse on non-int arg" do
    parser = Effect::Parser.new
    begin
      list = parser.parse("a b c(aaaaa,bbbbb)")
    rescue ex : Effect::ParseError
    end
  end

  it "doesnt parse on too many args" do
    parser = Effect::Parser.new
    begin
      list = parser.parse("jpgify(1,1,1,1,1,1,1)")
    rescue ex : Effect::ParseError
    end
  end

  it "negative ints work" do
    parser = Effect::Parser.new
    begin
      list = parser.parse("a b jpgify(-1)")
    rescue ex : Effect::ParseError
    end
  end
end
