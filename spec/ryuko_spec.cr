require "file_utils"
require "./spec_helper"
require "../src/context"

PREFIX = "https"
HOST   = "elixi.re"

# Original image is licensed under CC0 Public Domain.
IMG = "ogm10b70.jpg"

describe "Ryuko" do
  before_all do
    ctx = RyukoContext.new
    cache_dir = ctx.cache_dir

    FileUtils.rm_rf(cache_dir.to_s)
    FileUtils.mkdir_p(cache_dir.to_s)
  end

  it "serves well-known" do
    get "/.well-known/ryuko"
    response.status_code.should eq 204
  end

  it "requests an image with noop effect" do
    headers = HTTP::Headers.new
    headers["host"] = HOST
    get "/i/#{IMG}?effect=noop", headers
    response.status_code.should eq 200
    response.body.size.should_not eq 0
  end

  it "requests an image with jpgify effect" do
    headers = HTTP::Headers.new
    headers["host"] = HOST
    get "/i/#{IMG}?effect=jpgify", headers
    if response.status_code != 200
      puts "not 200, got body: #{response.body?}"
    end

    response.status_code.should eq 200
    response.body.size.should_not eq 0
  end

  it "works an image with many noop effects" do
    headers = HTTP::Headers.new
    headers["host"] = HOST
    get "/i/#{IMG}?effect=noop+noop+noop+noop+noop", headers
    response.status_code.should eq 200
    response.body.size.should_not eq 0
  end

  it "works an image with many jpgify effects" do
    headers = HTTP::Headers.new
    headers["host"] = HOST
    get "/i/#{IMG}?effect=jpgify+jpgify", headers
    response.status_code.should eq 200
    response.body.size.should_not eq 0
  end

  it "works an image with parametrized jpgify" do
    headers = HTTP::Headers.new
    headers["host"] = HOST
    get "/i/#{IMG}?effect=jpgify(30)+jpgify(20)+jpgify(1)", headers
    response.status_code.should eq 200
    response.body.size.should_not eq 0
  end

  it "does not work if parse error" do
    headers = HTTP::Headers.new
    headers["host"] = HOST
    get "/i/#{IMG}?effect=jpgify(3jAAAA", headers
    response.status_code.should eq 400
    response.body.size.should_not eq 0
  end

  it "works with all effects" do
    ctx = RyukoContext.new
    effects_dir = Dir.open(ctx.effects_dir)

    begin
      headers = HTTP::Headers.new
      headers["host"] = HOST

      effects_dir.each do |entry|
        if entry == "." || entry == ".."
          next
        end

        get "/i/#{IMG}?effect=#{entry}", headers
        if response.status_code != 200
          puts response.body.to_s
        end

        response.status_code.should eq 200
        response.body.size.should_not eq 0
      end
    ensure
      effects_dir.close
    end
  end
end
