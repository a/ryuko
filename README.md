# ryuko

image effect service for elixire instances.

## install

- [crystal](https://crystal-lang.org)

```
shards build --progress --release
```

## build ryuko effects

build dependencies (on the machine running `./build_effects.sh`):

- Rust

runtime dependencies (on the machine running ryuko):

- ImageMagick
- ffmpeg
- python3 with Pillow installed

```
./build_effects.sh
```

## running

```
env CRYSTAL_ENV=debug ELIXIRE_URL=http://localhost:9090 NGINX=1 ./bin/ryuko
```

### available env variables

- `CRYSTAL_ENV=debug` to set logger level to debug
- `ELIXIRE_URL` (required) to set the upstream elixire instance url
- `REAL_IP_HEADER=x-real-ip` (highly recommended) so that ryuko fills the header given by nginx.

### operational concerns

ryuko stores cached versions of the images with its effects applied in `/tmp/ryuko`.

there is a janitor task that cleans up that directory off images that are deemed
old enough (configurable via the `MAX_CACHE_MIN` variable).

it is important to check the mount flags set for `/tmp` so that it can't grow
over desirable limits.

#### nginx configuration

it is highly recommended to deploy ryuko behind a reverse proxy such as nginx.

assumes:

- elixire instance at `localhost`, port 8081
- ryuko instance at `localhost`, port 3000

```
    map $request_uri $proxy_pass_elixire_img_port {
        default 8081;

        # only match ryuko if "?effect" is in the original request url
        "~*\?effect" 3000;
    }

    server {
        <...>

        location / {
            proxy_set_header Host $http_host;
            proxy_pass http://localhost:8081;
        }

        location /i {
            proxy_set_header Host $http_host;

            # ryuko only acts on /i/ and even when only when an effect is needed
            proxy_pass http://127.0.0.1:$proxy_pass_elixire_img_port;
        }

    }

```
