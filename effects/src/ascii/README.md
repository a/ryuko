# ascii effect

`pyascii` is considered the upstream version.

There's also a zig project at this very root (see `src`), but it was an
experiment on speeding up python. It stays up as a possible vector for
future work.
