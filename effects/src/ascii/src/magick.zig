const std = @import("std");
const main = @import("./main.zig");

pub const c = @cImport({
    @cInclude("GraphicsMagick/wand/magick_wand.h");
    @cInclude("GraphicsMagick/wand/pixel_wand.h");
    @cInclude("GraphicsMagick/wand/drawing_wand.h");
    @cInclude("GraphicsMagick/magick/log.h");
});

fn setColor(pixel_wand: *c.PixelWand, color: [3]u8) !void {
    var status: c.MagickPassFail = c.MagickPass;

    // TODO: maybe cache this info
    var depth: c_ulong = undefined;
    _ = c.MagickGetQuantumDepth(&depth);

    var packet: c.PixelPacket = undefined;
    if (depth == 16) {
        packet.red = @intCast(u16, color[0]) * 257;
        packet.green = @intCast(u16, color[1]) * 257;
        packet.blue = @intCast(u16, color[2]) * 257;
        packet.opacity = 0;
    } else {
        packet.red = color[0];
        packet.green = color[1];
        packet.blue = color[2];
        packet.opacity = 0;
    }

    c.PixelSetQuantumColor(pixel_wand, &packet);

    if (depth == 16) {
        const red = c.PixelGetRedQuantum(pixel_wand);
        const green = c.PixelGetGreenQuantum(pixel_wand);
        const blue = c.PixelGetBlueQuantum(pixel_wand);
        std.debug.assert(red == packet.red);
        std.debug.assert(green >= packet.green);
        std.debug.assert(blue >= packet.blue);
    }
}

test "set color works" {
    var pixel_wand = c.NewPixelWand().?;
    defer c.DestroyPixelWand(pixel_wand);

    var depth: c_ulong = undefined;
    _ = c.MagickGetQuantumDepth(&depth);

    try setColor(pixel_wand, [3]u8{ 00, 255, 255 });

    const red = c.PixelGetRedQuantum(pixel_wand);
    const green = c.PixelGetGreenQuantum(pixel_wand);
    const blue = c.PixelGetBlueQuantum(pixel_wand);

    std.debug.warn("{} {} {}\n", .{ red, green, blue });

    std.debug.assert(red == 0);
    std.debug.assert(green >= 255);
    std.debug.assert(blue >= 255);
}

fn logShit(exc: c.ExceptionType, txt: [*c]const u8) callconv(.C) void {
    //std.log.info("{} {s}", .{ exc, txt });
}

const SpriteMap = std.AutoHashMap(u8, *c.MagickWand);

pub const MagickBackend = struct {
    const Context = struct {
        allocator: *std.mem.Allocator,
        sprite_map: SpriteMap,

        magick_wand: *c.MagickWand,
        spritesheet_magick_wand: *c.MagickWand,
        drawing_wand: *c.DrawingWand,
        pixel_wand: *c.PixelWand,
    };

    pub fn initAPI() void {
        c.InitializeMagick(null);
        _ = c.SetLogEventMask("Warning,Error,Exception,FatalError");
        c.SetLogMethod(logShit);

        // calling log functions segfaults without an initialization
        // but also you have to initialize after setting logs
        // this is confusing
        //
        // but such is life
        c.InitializeMagick(null);
    }

    pub fn deinitAPI() void {}

    pub fn createContext(allocator: *std.mem.Allocator) !Context {
        var pixel_wand = c.NewPixelWand().?;
        var drawing_wand = c.NewDrawingWand().?;
        var magick_wand = c.NewMagickWand().?;

        var spritesheet_magick_wand = c.NewMagickWand().?;
        return Context{
            .allocator = allocator,
            .sprite_map = SpriteMap.init(allocator),

            .magick_wand = magick_wand,
            .drawing_wand = drawing_wand,
            .pixel_wand = pixel_wand,
            .spritesheet_magick_wand = spritesheet_magick_wand,
        };
    }

    pub fn deinitContext(ctx: *Context) void {
        c.DestroyPixelWand(ctx.pixel_wand);
        c.DestroyDrawingWand(ctx.drawing_wand);
        _ = c.DestroyMagickWand(ctx.magick_wand);
        _ = c.DestroyMagickWand(ctx.spritesheet_magick_wand);

        var it = ctx.sprite_map.iterator();
        while (it.next()) |entry| {
            _ = c.DestroyMagickWand(entry.value);
        }

        ctx.sprite_map.deinit();
    }

    pub fn initCanvas(
        ctx: *Context,
        width: usize,
        height: usize,
    ) !void {
        var status: c.MagickPassFail = c.MagickPass;

        status = c.MagickSetSize(ctx.magick_wand, width, height);
        if (status == c.MagickFail) return error.SetSizeError;

        // set image background to red so that drawing issues are easier to notice
        status = c.MagickReadImage(ctx.magick_wand, "xc:red");
        if (status == c.MagickFail) return error.ReadImageError;

        status = c.MagickSetImageColorspace(
            ctx.magick_wand,
            c.ColorspaceType.RGBColorspace,
        );
        if (status == c.MagickFail) return error.ColorspaceError;

        // set fill color, under color DEFAULTS so we know if we fucked up with
        // very high tier colors
        // status = c.PixelSetColor(ctx.pixel_wand, "red");
        // if (status == c.MagickFail) return error.SetColorError;
        // c.MagickDrawSetFillColor(ctx.drawing_wand, ctx.pixel_wand);

        // status = c.PixelSetColor(ctx.pixel_wand, "#ffff00");
        // if (status == c.MagickFail) return error.SetColorError;
        // c.MagickDrawSetTextUnderColor(ctx.drawing_wand, ctx.pixel_wand);

        // good text things
        // c.MagickDrawSetStrokeAntialias(ctx.drawing_wand, 1);
        // c.MagickDrawSetTextAntialias(ctx.drawing_wand, 1);

        // // initialize ctx.drawing wand with the font
        // c.MagickDrawSetFont(ctx.drawing_wand, dimensions.font_path_cstr.ptr);
        // c.MagickDrawSetFontSize(ctx.drawing_wand, @intToFloat(f64, font_pt_size));
        // c.MagickDrawSetTextDecoration(ctx.drawing_wand, c.DecorationType.NoDecoration);
    }

    pub fn initSpritesheet(
        ctx: *Context,
        metadata: main.SpritesheetMetadata,
        spritesheet_path_cstr: [:0]const u8,
    ) !void {
        var status: c.MagickPassFail = c.MagickPass;

        status = c.MagickReadImage(ctx.spritesheet_magick_wand, spritesheet_path_cstr);
        if (status == c.MagickFail) return error.ReadImageError;

        const font_width = metadata.font_dimensions[0];
        const font_height = metadata.font_dimensions[1];

        for (metadata.letter_coords) |letter_position| {
            const letter = letter_position.letter[0];

            // clone root spritesheet, create a crop of it for each letter
            const sprite_magick_wand = c.CloneMagickWand(ctx.spritesheet_magick_wand).?;

            status = c.MagickCropImage(
                sprite_magick_wand,
                font_width,
                font_height,
                @intCast(c_long, letter_position.x),
                @intCast(c_long, letter_position.y),
            );
            if (status == c.MagickFail) return error.SpriteLoadError;

            std.log.info("init sprite, {s} {}", .{ letter_position.letter, sprite_magick_wand });
            try ctx.sprite_map.put(letter, sprite_magick_wand);
        }
    }

    pub fn setCurrentColors(ctx: *Context, foreground: [3]u8, background: [3]u8) !void {}

    pub fn drawText(ctx: *Context, x: usize, y: usize, text: []const u8, text_cstr: [:0]const u8) !void {
        for (text) |char| {
            const sprite_wand = ctx.sprite_map.get(text[0]);
            if (c.MagickCompositeImage(
                ctx.magick_wand,
                sprite_wand,
                c.CompositeOperator.OverCompositeOp,
                @intCast(c_long, x),
                @intCast(c_long, y),
            ) == -1) return error.CompositeError;
        }
    }

    pub fn finishDrawing(ctx: *Context) !void {
        if (c.MagickDrawImage(ctx.magick_wand, ctx.drawing_wand) == c.MagickFail) {
            var severity: c.ExceptionType = undefined;
            var description = c.MagickGetException(ctx.magick_wand, &severity);

            std.log.warn("error drawing: {s} {}", .{ description, severity });

            return error.DrawError;
        }
    }

    pub fn saveImage(ctx: *Context, path_cstr: [:0]const u8) !void {
        if (c.MagickWriteImage(ctx.magick_wand, path_cstr.ptr) != 1) {
            var severity: c.ExceptionType = undefined;
            var description = c.MagickGetException(ctx.magick_wand, &severity);

            std.log.warn("error saving: {s} {}", .{ description, severity });

            return error.MagickWriteFail;
        }
    }
};
