require "ini"
require "digest"
require "logger"
require "kemal"

require "expiring_hash"

alias FailureCache = ExpiringHash(String, Int32)

class RyukoContext
  property log = Logger.new(STDOUT)

  @EFFECTS_DIR = Path.new("./effects/bin")

  property cache_dir : Path

  property failure_cache : FailureCache

  def initialize
    elixire_url = ENV["ELIXIRE_URL"]
    checksum = Digest::CRC32.checksum(elixire_url)

    actual_cache_dir = Path.new("/tmp/ryuko") / checksum.to_s
    @cache_dir = actual_cache_dir

    @failure_cache = FailureCache.new(1500, 5.minutes)
  end

  def effects_dir
    return @EFFECTS_DIR
  end

  def setup
    crystal_env = ENV["CRYSTAL_ENV"]? || "info"

    if crystal_env.downcase == "debug"
      log.info("setting logger to debug")
      log.level = Logger::DEBUG
    end
  end

  def check_port(config)
    elixire_url = ENV["ELIXIRE_URL"]
    resp = HTTP::Client.get(elixire_url + "/.well-known/ryuko")
    if resp.status_code == 204
      @log.error("ELIXIRE_URL points to ryuko. This would cause an infinite loop. Stopping.")
      Kemal.stop
    end
  end
end
