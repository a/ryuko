require "http"
require "file_utils"
require "mime"

require "kemal"

require "./discord"
require "./effect"

HEADERS_TO_COPY = [
  "content-type",
  "content-length",
  "content-disposition",
  "content-security-policy",
  "accept-ranges",

  "access-control-allow-origin",
  "access-control-allow-headers",
  "access-control-expose-headers",
  "access-control-allow-methods",

  "x-ratelimit-limit",
  "x-ratelimit-remaining",
  "x-ratelimit-reset",

  "last-modified", "cache-control", "expires", "etag", "vary",
]

# return the headers and url that should be used in the call to the
# upstream elixire instance
def setup_request(env)
  file = env.params.url["shortname"]

  elixire_url = ENV["ELIXIRE_URL"]

  headers = HTTP::Headers.new

  # Host header contains the domain
  headers["host"] = env.request.headers["host"]

  # The configuration in REAL_IP_HEADER should be the same REAL_IP_HEADER in elixire v3.
  # in elixire v2, set REAL_IP_HEADER to "cf-connecting-ip"

  real_ip_header = ENV["REAL_IP_HEADER"]?
  unless real_ip_header.nil?
    headers[real_ip_header] = env.request.headers[real_ip_header]
  end

  final_url = elixire_url + "/i/" + file
  log = env.get("log").as(Logger)
  log.debug("request headers: #{headers}")
  return final_url, headers
end

# copy headers from a given response to the given env.response
def copy_headers_to_env(response : HTTP::Client::Response, env)
  HEADERS_TO_COPY.each do |header|
    if response.headers.has_key? header
      env.response.headers[header] = response.headers[header]
    end
  end
end

head "/i/:file" do |env|
  log = env.get("log").as(Logger)
  final_url, headers = setup_request env

  HTTP::Client.head(final_url, headers: headers) do |response|
    copy_headers_to_env(response, env)

    env.response.status_code = response.status_code
    puts "response headers head: #{response.headers}"
    puts "content length: #{response.headers["content-length"]}"
    env.response.content_length = response.headers["content-length"].to_u64
  end
end

def bad_request(env, message : String)
  ctx = env.get("ctx").as(RyukoContext)
  ctx.log.info("bad request: #{message}")
  env.response.status_code = 400
  env.response.headers["content-type"] = "application/json"

  encoded_json =
    {error: true, message: message}.to_json.encode("UTF-8")
  env.response.content_length = encoded_json.size
  env.response.write(encoded_json)
end

# get the full path inside the cache directory given a shortname
def get_full_cache_path(ctx, file_name : String, list) : Path
  FileUtils.mkdir_p(ctx.cache_dir.to_s)

  shortname, extension = file_name.split(".")

  # TODO use libmagic here?
  mimetype = MIME.from_extension(".#{extension}")

  ctx.cache_dir / "#{shortname}_#{list.serialize}.#{extension}"
end

def apply_effect(
  ctx,
  upstream_body_io : IO,
  response : HTTP::Server::Response,
  file_name : String,
  list : Effect::CommandList
)
  full_path = get_full_cache_path(ctx, file_name, list)

  # if it exits in cache, we use it, if not we need to generate it
  #
  # this is done by feeding what elixire gave to us on full_path
  # then give full_path as the RYUKO_INPUT_FILE variable when executing
  # the effect itself.
  begin
    File.open(full_path, mode: "rb") do |file|
      ctx.log.debug("using cached new file #{full_path}")
      copy_io(ctx.log, file, response)
    end
  rescue ex : File::NotFoundError
    ctx.log.debug("generating #{full_path}")

    # copy upstream_body_io to it
    file = File.new(full_path, mode: "wb")
    begin
      IO.copy(upstream_body_io, file)
    ensure
      file.close
    end

    Effect::Runner.run(ctx, full_path, list)
    resulting_file = File.open(full_path, mode: "rb")

    # effects can grow or shrink the file, mirror the end result on
    # content-length so the browser doesnt read less than it must
    info = File.info(full_path)
    response.content_length = info.size

    copy_io(ctx.log, resulting_file, response)
  end
end

# copy io between two IO objects
def copy_io(log : Logger, in_io : IO, out_io : IO)
  begin
    log.debug("copy io #{in_io} => #{out_io}")
    IO.copy(in_io, out_io)
  rescue ex : IO::Error
    log.debug("Failed to copy IO: #{ex.inspect_with_backtrace}")
  end
end

def fetch_effect_from_env(env) : Effect::CommandList?
  raw_effect_string = env.request.query_params["effect"]

  parser = Effect::Parser.new

  begin
    parser.parse(raw_effect_string)
  rescue ex : Effect::ParseError
    # map ParseError to a 400
    bad_request(env, "#{ex}")
    nil
  end
end

get "/i/:shortname" do |env|
  log = env.get("log").as(Logger)
  ctx = env.get("ctx").as(RyukoContext)

  if should_twitter_card?(env)
    inject_twitter_card(env)
    next
  end

  final_url, headers = setup_request env

  list = fetch_effect_from_env(env)
  if list.nil?
    next
  end

  begin
    Effect::Runner.validate(ctx, list)
  rescue ex : Effect::ValidationError
    bad_request(env, "validation error: #{ex}")
    next
  end

  # try to find it in failure cache first
  shortname = env.params.url["shortname"]
  failure = ctx.failure_cache[shortname]?
  unless failure.nil?
    env.response.status_code = failure
    env.response.headers["content-type"] = "text/plain"
    env.response.write("status: #{failure}".encode("UTF-8"))
    next
  end

  headers["accept"] = "*/*"

  # even though ryuko caches the content of the effect run, that caching does
  # not affect this call. every file lookup will be checked on the upstream
  # instance and if it returns a 404, we remove the file from our cache, and
  # keep that 404 cached (see failure_cache).
  HTTP::Client.get(final_url, headers: headers) do |response|
    copy_headers_to_env(response, env)
    env.response.status_code = response.status_code

    if response.status_code == 200
      apply_effect(
        ctx, response.body_io, env.response,
        shortname, list
      )
    elsif response.status_code == 404
      ctx.failure_cache[shortname] = 404
      delete_from_cache(log, ctx, shortname)
    elsif response.status_code == 500
      raise response.body_io.gets_to_end
    else
      ctx.failure_cache[shortname] = response.status_code
      log.warn("unexpected status code #{response.status_code}")
      copy_io(log, response.body_io, env.response)
    end
  end
end

def delete_from_cache(log, ctx, filename : String)
  # assumes unix
  shortname, ext = filename.split(".")
  cached_files = Dir.glob(ctx.cache_dir.to_s + "/#{shortname}_*.#{ext}")

  cached_files.each do |path_str|
    log.debug("deleting '#{path_str}'")
    File.delete(path_str)
  end

  log.debug("deleted #{cached_files.size} cached files")
end
