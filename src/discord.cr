# twitter card embed helper

def should_twitter_card?(env)
  is_raw = env.params.query["raw"]?
  is_image = false
  [".png", ".jpg", ".jpeg", ".webp"].each do |extension|
    if env.request.path.ends_with? extension
      is_image = true
    end
  end

  user_agent = env.request.headers["user-agent"]? || ""
  is_discordbot = user_agent.includes? "Discordbot"

  return is_discordbot && is_image && !is_raw
end

def inject_twitter_card(env)
  host = env.request.host
  path = env.request.path
  query = env.request.query

  proto = if ENV["HTTPS"]?
            "https"
          else
            "http"
          end

  # TODO: maybe escape query here?
  raw_url = "#{proto}://#{host}#{path}?#{query}&raw=1"

  body = "
  <html>
     <head>
         <meta property=\"twitter:card\" content=\"summary_large_image\">
         <meta property=\"twitter:image\" content=\"#{raw_url}\">
     </head>
  </html>
  "

  env.response.status_code = 200
  env.response.headers["content-type"] = "text/html"
  env.response.write(body.encode("UTF-8"))
end
