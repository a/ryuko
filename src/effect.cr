module Effect
  # Any effect that has support for arguments should be registered here
  # Effects not here are assumed to not have any parameters
  PARAMETIZED_EFFECTS = {
    "brightness" => 1,
    "contrast"   => 1,
    "hue"        => 1,
    "jpgify"     => 1,
    "hypercam2"  => 1,
    "pixelate"   => 1,
    "rotate"     => 1,
    "saturation" => 1,
    "ascii"      => 1,
    "scale"      => 2,
  }

  class Command
    property name : String
    property args : Array(String)

    def initialize(@name : String, @args : Array(String))
    end

    def serialize : String
      res = "#{@name}"
      @args.each do |arg|
        res += ",#{arg}"
      end
      res
    end
  end

  class CommandList
    property commands : Array(Command)

    def initialize(@commands : Array(Command))
    end

    def serialize
      result = ""
      @commands.each do |command|
        result = result + "-#{command.serialize}"
      end
      result[1..]
    end
  end

  class ParseError < Exception
  end

  class Parser
    def parse(text : String)
      list = CommandList.new(Array(Command).new)

      text.split(' ').each do |call|
        if list.commands.size > 10
          raise ParseError.new("Too many effects")
        end

        function_form = call.match(/^([a-z0-9_]+)\((-?\d+(?:,-?\d+)*)\)$/)
        named_form = call.match(/^([a-z0-9_]+)$/)

        if function_form.nil? && named_form.nil?
          raise ParseError.new("failed to parse #{call}")
        end

        unless function_form.nil?
          # this is a full effect call (d(1))
          #
          # - total amount of arguments must not pass effect_arity
          # - arguments must be numbers
          effect_name = function_form[1]
          effect_arity = PARAMETIZED_EFFECTS[effect_name]? || 0

          args_string = function_form[2].split(',')

          if args_string.size > effect_arity
            raise ParseError.new("too many arguments in call to #{effect_name} (max is #{effect_arity})")
          end

          args_string.each_with_index do |arg_string, index|
            begin
              arg_string.to_i32
            rescue ex : ArgumentError
              raise ParseError.new("argument #{index} in call to #{function_form[1]} is not a number")
            end
          end

          list.commands.push(Command.new(function_form[1], args_string))
        end

        unless named_form.nil?
          # this is just the name form (d), assume arguments is empty
          # effects must always have reasonable argument defaults
          args = Array(String).new
          list.commands.push(Command.new(named_form[1], args))
        end
      end

      list
    end
  end

  class ValidationError < Exception
  end

  class Runner
    # Run the given list of commands, applying them to the given file.
    def self.run(ctx, image_path : Path, list)
      list.commands.each do |command|
        effect_path = ctx.effects_dir / "#{command.name}"

        # execute effect script
        stderr = IO::Memory.new
        stdout = IO::Memory.new

        # TODO: how do i make the env of this just shit
        # (without secrets coming from parent)
        ctx.log.debug("Running effect #{command.name}")

        process = Process.new(
          effect_path.to_s,
          args: [image_path.to_s] + command.args,
          error: stderr, output: stdout
        )
        ctx.log.debug("out: #{stdout.to_s}")
        ctx.log.debug("err: #{stderr.to_s}")

        status = process.wait
        if !status.success?
          ctx.log.debug("no success, got status #{status.exit_status} '#{stderr.to_s}'")
          raise "failed to run effect: #{stderr.to_s}"
        end
      end
    end

    # Validate the given list of commands makes sense for the runner
    def self.validate(ctx, list : CommandList)
      list.commands.each do |command|
        possible_path = ctx.effects_dir / command.name
        begin
          f = File.open(possible_path, mode: "r")
        rescue ex : File::NotFoundError
          raise ValidationError.new("Effect not found: #{command.name}")
        ensure
          unless f.nil?
            f.close
          end
        end
      end
    end
  end
end
