#!/bin/sh

set -o nounset
set -o errexit

target="./effects/bin"

maybe_rm_fv() {
    if [ -d "$1" ]; then
        rm -rv "$1"
    fi
}

maybe_rm_fv "$target"
mkdir -vp "$target"

for effect_source_folder in ./effects/src/*; do
    echo "BUILD : $(basename $effect_source_folder)"

    if [ -f "$effect_source_folder/Makefile" ]; then
        # always force it to refresh
        maybe_rm_fv "$effect_source_folder/bin"

        cd "$effect_source_folder"
        make
        cd ../../..
        cp -v $effect_source_folder/bin/* ./effects/bin/
    else
        cp -v $effect_source_folder/* ./effects/bin/
    fi
    echo ""
done

chmod -v +x ./effects/bin/*
